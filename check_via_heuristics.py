#!/usr/bin/python3

import asyncio
import datetime
import re
import sys
import traceback

import httpx
import pandas


def get_app_author(row):
    repo_url = row.get("extra.repository", "")
    if repo_url.startswith("https://code.qt.io/"):
        return "Qt Company"
    elif repo_url.startswith("https://invent.kde.org/plasma-mobile/"):
        return "Plasma Mobile Developers"
    elif repo_url.startswith("https://invent.kde.org/"):
        return "KDE Community"
    elif repo_url.startswith("https://gitlab.com/postmarketOS/"):
        return "postmarketOS Developers"
    elif repo_url.startswith("https://gitlab.gnome.org/"):
        return "GNOME Developers"
    else:
        return repo_url.split("/")[-2].lower().replace("~", "").replace("_", "-")


def is_github_or_gitea(s):
    github_urls = [
        "https://code.smolnet.org/",
        "https://codeberg.org/",
        "https://github.com/",
    ]
    return any(s.startswith(url) for url in github_urls)


def is_gitlab(s):
    gitlab_urls = [
        "https://dev.gajim.org/",
        "https://framagit.org/",
        "https://git.eyecreate.org/",
        "https://gitlab.com/",
        "https://gitlab.freedesktop.org/",
        "https://gitlab.gnome.org/",
        "https://gitlab.shinice.net/",
        "https://invent.kde.org/",
        "https://salsa.debian.org/",
        "https://source.puri.sm/",
    ]
    return any(s.startswith(url) for url in gitlab_urls)


def is_sourcehut(s):
    return re.match(r"^https?://(?:[^.]+\.)?sr.ht/", s) is not None


def is_flathub(s):
    return s.startswith("https://flathub.org")


def get_bugtracker(row):
    repo_url = row.get("extra.repository", "")
    if repo_url[-1] == "/":
        repo_url = repo_url[:-1]

    if is_github_or_gitea(repo_url):
        return repo_url + "/issues/"
    elif is_gitlab(repo_url):
        return repo_url + "/-/issues/"
    elif is_sourcehut(repo_url):
        return re.sub(r"^https?://(?:[^.]+\.)?sr\.ht/(.*)$", r"https://todo.sr.ht/\g<1>", repo_url).lower()
    raise Exception(f"Could not determine bugtracker based on repository {repo_url}")


def get_flathub(row):
    return row.get("extra.flatpak", "") if is_flathub(row.get("extra.flatpak", "")) else ""


async def check(client, row, update=False):
    item_name = row.get("extra.app_id") or row.get("name", "")

    properties = [
        {"apps_csv_column": "[taxonomies.app_author]", "handler": get_app_author},
        {"apps_csv_column": "extra.bugtracker", "handler": get_bugtracker},
        {"apps_csv_column": "extra.flathub", "handler": get_flathub},
    ]
    found = False
    for property in properties:
        if row.get(property["apps_csv_column"]):
            continue  # ignore non-empty fields

        try:
            found_entry = property["handler"](row).strip()
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_csv_column"]}: {e}', file=sys.stderr)
            # traceback.print_exception(e, file=sys.stderr)
            continue

        found = True
        print(f'{item_name}: {property["apps_csv_column"]}: {found_entry}', file=sys.stderr)
        if update:
            row[property["apps_csv_column"]] = found_entry
            row["updated_date"] = str(datetime.date.today())
            row["extra.updated_by"] = "script"

    return found


async def run(filename, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        df = pandas.read_csv(filename, keep_default_na=False)

        tasks = []
        for _, row in df.iterrows():
            tasks.append(asyncio.ensure_future(check(client, row, update)))
        found = any(await asyncio.gather(*tasks))

        if found and update:
            print(f"Writing changes to {filename}")
            df.to_csv(filename, index=False)
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FILENAME")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_file = sys.argv[2]
    found = await run(apps_file, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_file}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
