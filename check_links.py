#!/usr/bin/python3

import asyncio
import datetime
import io
import sys
import urllib.parse

import httpx
import pandas
from PIL import Image


def is_link(url):
    return url.strip().startswith("http")


async def check_link_reachable(client, url):
    try:
        response = await client.head(url, follow_redirects=False)
        code = response.status_code
        reason = response.reason_phrase.capitalize()
        if code == 200:
            return None
        elif code == 301 or code == 308:
            location = this_location = urllib.parse.urljoin(url, response.headers['Location'])
            message = f"Permanent redirect to {location}"
            # recursive resolution
            if (result := await check_link_reachable(client, location)) is not None:  # reachable
                if result[0] is not None:  # redirect url
                    location, message = result
                    message += f" via {this_location}"
            return (location, message)
        elif code == 302 or code == 307:
            location = urllib.parse.urljoin(url, response.headers['Location'])
            return (None, f"Temporary redirect ({code}) to {location}")
        return (None, f"{reason} ({code})")
    except Exception as e:
        return (None, f"Error occurred checking {url}: {e}")


async def check_link_https(client, url):
    if url.startswith("https"):
        return None

    suggestion = url.replace("http", "https")
    if await check_link_reachable(client, suggestion) is not None:  # not reachable
        suggestion = None

    return (suggestion, "Not HTTPS!")


async def check_field(client, field):
    if not is_link(field.strip()):
        return field

    checkers = [check_link_reachable, check_link_https]
    links = [link.strip() for link in field.strip().split(",") if is_link(link)]
    for i, _ in enumerate(links):
        for checker in checkers:
            result = await checker(client, links[i])
            if not result:  # reachable
                continue
            found_url, result_message = result
            print(f"{links[i]}: {result_message}", file=sys.stderr)
            if found_url:
                links[i] = found_url
    return ",".join(links)


async def check_screenshot(client, image_url):
    try:
        async with client.stream("GET", image_url) as response:
            content_type = response.headers["content-type"]
            if not content_type.startswith("image/"):
                return f'{image_url}: Invalid image type "{content_type}"'

            image = Image.open(io.BytesIO(await response.aread()))
            width, height = image.size
            if width > height:
                return f"{image_url}: Landscape image with dimensions {width}x{height} detected but expecting mobile images to be portrait ones"
        return None
    except Exception as e:
        return f"{image_url}: Error occurred: {e}"


# check links for error
async def check(client, row, update=False):
    found = False
    for col in row.index:
        field = await check_field(client, row[col])
        if field != row[col]:  # found
            found = True
            if update:
                row[col] = field
                row["updated_date"] = str(datetime.date.today())
                row["extra.updated_by"] = "script"

    for screenshot in filter(None, row["[extra.screenshots]"].split(",")):
        screenshot_error = await check_screenshot(client, screenshot)
        if screenshot_error is not None:
            print(screenshot_error, file=sys.stderr)

    return found


async def run(filename, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        df = pandas.read_csv(filename, keep_default_na=False)

        tasks = []
        for _, row in df.iterrows():
            tasks.append(asyncio.ensure_future(check(client, row, update)))
        found = any(await asyncio.gather(*tasks))

        if found and update:
            print(f"Writing changes to {filename}")
            df.to_csv(filename, index=False)
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FILENAME")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_file = sys.argv[2]
    found = await run(apps_file, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_file}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
